//
//  Array.swift
//  DataStructures
//
//  Created by TranHoangThanh on 6/23/21.
//

import Foundation

class ArrayDemo {
    var items : Array<Int>!
    var count = 0
    
    init(length : Int) {
        items = Array(repeating: 0, count: length)
    }
    
    func resizeIfRequired(){
        if (items.count == count) {
            var newItems = Array(repeating: 0, count: count * 2)
            for i in 0..<count {
                newItems[i] = items[i]
            }
            items = newItems
        }
    }
    
    func reverse(){
        var newItems = Array(repeating: 0, count: count)
        for i in 0..<count {
            print("count - i - 1",count - i - 1)
            newItems[i] = items[count - i - 1]
        }
        items = newItems
    }
    
    func insert(item : Int) {
        resizeIfRequired()
        items[count] = item
        count += 1
        print("count",count)
    }
    
    func insertAt(item : Int, index : Int) {
        if index < 0 || index > count {
            print("IllegalArgumentException")
            return
        }
        resizeIfRequired();
       
        
        
         for i in stride(from: count-1, through: index, by: -1) {
            items[i+1] = items[i]
         }
//        for i in (index...count-1).reversed() {
//            items[i+1] = items[i]
//        }

        items[index] = item
        count += 1
    }
    
    func max() -> Int {
        var max = 0
        for i in 0...count-1 {
            if items[i] > max {
                max = items[i]
            }
        }
        return max
    }
    
    func removeAt(index : Int) {
        if index < 0 || index > count {
            print("IllegalArgumentException")
            return
        }
        
        for i in index..<count {
            items[i] = items[i+1]
        }
        
        count -= 1
    }
    
    func indexOf(item :Int) -> Int {
        for i in 0..<count {
            if items[i] == item {
                return i
            }
        }
        return -1
    }
    
    func getValueIndex(index :Int) -> Int {
        for i in 0..<count {
            if i == index {
                return items[i]
            }
        }
        return -1
    }

    func intersect(other : ArrayDemo) -> ArrayDemo {
        let intersection = ArrayDemo(length: count)
        for item  in items {
            if other.indexOf(item: item) >= 0 {
                intersection.insert(item: item)
            }
        }
        return intersection
    }
    
}


func testArray(){
    let arr = ArrayDemo(length: 5)
    arr.insert(item: 1)
    arr.insert(item: 2)
    arr.insert(item: 3)
    arr.insert(item: 4)
  //  arr.removeAt(index: 2)
    arr.insertAt(item: 7, index: 2)
//    let arr2 = ArrayDemo(length: 5)
//    arr2.insert(item: 3)
//    arr2.insert(item: 4)
//    arr2.insert(item: 12)
//    arr2.insert(item: 6)
//    arr2.insert(item: 7)
//    arr2.insert(item: 9)
//    arr2.insert(item: 11)
    
   // print(arr.intersect(other: arr2).items ?? <#default value#>)
  //  arr.indexOf(item: 4)
    //arr.insertAt(item: 10, index: 5)
  //  print("max arr", arr.max())
    print(arr.indexOf(item: 9))
    print("arr", arr.items ?? [])
}
