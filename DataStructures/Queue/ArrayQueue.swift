//
//  ArrayQueue.swift
//  DataStructures
//
//  Created by TranHoangThanh on 6/23/21.
//

import Foundation

class ArrayQueue {
    var items : Array<Int>!
    var rear = 0
    var front = 0
    var count = 0
    init(capacity : Int) {
        items = Array(repeating: 0, count: capacity)
    }
    
    func enqueue(item : Int) {
        if (isFull()) {
            print("IllegalStateException")
            return
        }
        items[rear] = item;
        rear = (rear + 1) % items.count;
        count += 1
    }
    
    func dequeue() -> Int {
        if (isEmpty()){
            print("IllegalStateException")
            return -1
        }
        let item = items[front];
        items[front] = 0;
        front = (front + 1) % items.count;
        count -= 1;
        
        return item;
    }
    func isEmpty() -> Bool {
        return count == 0;
    }
    func isFull() -> Bool {
        return count == items.count;
    }
}

func testArrayQueue(){
    let queue = ArrayQueue(capacity: 5)
    queue.enqueue(item: 1)
    queue.enqueue(item: 5)
    queue.enqueue(item: 4)
    queue.enqueue(item: 6)
    queue.enqueue(item: 2)
 //   print(queue.dequeue())
    print(queue.items)
}
