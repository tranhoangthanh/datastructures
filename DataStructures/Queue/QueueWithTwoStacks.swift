//
//  QueueWithTwoStacks.swift
//  DataStructures
//
//  Created by thanh on 23/06/2021.
//

import Foundation

class QueueWithTwoStacks {
    var stack1 : Stack = Stack(length: 5)
    var stack2 : Stack = Stack(length: 5)
    
    // O(1)
    func enqueue(item : Int) {
        stack1.push(item: item)
    }
    
    //O(n)
    func dequeue() -> Int {
        if (isEmpty()) {
            print("IllegalStateException")
            return -1
        }
        moveStack1ToStack2()
        return stack2.pop()
           
    }
    
    func moveStack1ToStack2() {
        if (stack2.isEmpty()) {
            while (!stack1.isEmpty()) {
                stack2.push(item: stack1.pop());
            }
        }
    }
    
    func peek() -> Int {
        if (isEmpty()) {
            print("IllegalStateException")
            return -1
        }
      
       moveStack1ToStack2();

       return stack2.peek();
     }
    
    func isEmpty() -> Bool {
        return stack1.isEmpty() && stack2.isEmpty();
    }
}


func testQueueWithTwoStacks(){
    let queue = QueueWithTwoStacks()
    queue.enqueue(item: 1)
    queue.enqueue(item: 2)
    queue.enqueue(item: 3)
    queue.enqueue(item: 4)
    print(queue.dequeue())
    print(queue.dequeue())
//    print(queue.dequeue())
    print(queue.stack2.items)
    print(queue.stack1.items)
}
