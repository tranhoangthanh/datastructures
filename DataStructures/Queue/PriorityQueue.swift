//
//  PriorityQueue.swift
//  DataStructures
//
//  Created by thanh on 23/06/2021.
//

import Foundation

class PriorityQueue  {
    
    var items : Array<Int> = Array(repeating: 0, count: 5)
    var count = 0
    
    // O(n)
    func add(item : Int) {
        if (isFull()) {
            print("IllegalStateException")
            return
         }
        let i = shiftItemsToInsert(item: item);
        items[i] = item;
        count += 1
    }
    
    func shiftItemsToInsert(item : Int) -> Int {
        let value = count - 1
        for i in stride(from: value, through: 0, by: -1) {
            if items[i+1] > item {
                items[i + 1] = items[i];
            } else {
                break
            }
        }
    
        return value + 1

    }
    
    // O(1)
      func remove() -> Int {
        if (isEmpty()) {
            print("IllegalStateException")
            return -1
        }
      
        count = count-1

        return items[count];
      }
    
    func isFull() -> Bool {
        return count == items.count;
      }
    
    func isEmpty() -> Bool {
        return count == 0;
      }
}


func testPriorityQueue() {
    let queue = PriorityQueue()
    queue.add(item: 1)
    queue.add(item: 5)
    queue.add(item: 4)
    queue.add(item: 6)
    queue.add(item: 2)

    print(queue.remove())
    print(queue.items)
}
