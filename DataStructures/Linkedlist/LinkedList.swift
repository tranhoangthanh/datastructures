//
//  LinkedList.swift
//  DataStructures
//
//  Created by thanh on 24/06/2021.
//

import Foundation

class LinkedList {
    var first : Node?
    var last : Node?
    var size : Int = 0
    
    func addLast(_ item : Int) {
        let node = Node(item);

        if (isEmpty()) {
            first  = node;
            last = node
        } else {
            last?.next = node;
            last = node;
        }
       size += 1
    }
    
    func addFirst(_ item : Int) {
        let node = Node(item);

        if (isEmpty()) {
            first  = node;
            last = node
        } else {
            node.next = first
            first = node;
        }

        size += 1
      }

    
    func isEmpty() -> Bool {
        return first == nil;
    }
    
     func getNode(_ index : Int) -> Node? {
        if index == 0 {
            return first
        } else {
            var node = first?.next
            for _ in 1..<index {
                node = node?.next
                if node == nil {break}
            }
            return node
        }
     }
    
    func setNode(_ value : Int , _ index : Int) {
        let newNode = Node(value)
        if index == 0 {
            newNode.next = first
            first = newNode
        } else {
            let prev = self.getNode(index-1)
            let next = prev?.next
            print(prev?.value)
            print(next?.value)
            prev?.next = newNode
            newNode.next = next
//            newNode.previous = prev
//            newNode.next = prev?.next
            
        }
    }
    
//    func removeAt(node : Node) -> Int {
//        let prev = node.previous
//        let next = node.next
//        if let prev = prev {
//            prev.next = next
//        } else {
//            head = next
//        }
//        
//        next?.previous = prev
//        node.previous = nil
//        node.next = nil
//        return node.value
//    }
//    
    
    
    
    var printLL : String {
        var stringArray = "["
        guard var node = first else {
            return stringArray + "]"
        }
        
        while let next = node.next {
            stringArray += "\(node.value),"
            node = next
        }
        
        stringArray += "\(node.value)"
        
        return stringArray + "]"
        
    }
    
    
    func indexOf(_ item : Int) -> Int {
        var index = 0;
        var current = first;
        while (current != nil) {
         if (current?.value == item) { return index} ;
            current = current?.next;
            index += 1
        }
        return -1;
      }
    
    func contains(item : Int) -> Bool{
        return indexOf(item) != -1;
    }
    
    func removeFirst() {
        if (isEmpty()) {
            print("NoSuchElementException")
            return
        }
       
       if (first?.value == last?.value) {
          last = nil;
          first = nil
        }
        else {
          let second = first?.next;
          first?.next = nil;
          first = second;
        }

        size -= 1
      }
    
    
    func removeLast() {
        if (isEmpty()) {
            print("NoSuchElementException")
            return
        }
         

        if (first?.value == last?.value) {
            last = nil;
            first = nil
        }
        
        else {
          let previous = getPrevious(last);
          last = previous;
          last?.next = nil;
        }

        size -= 1;
      }
    
    func getPrevious(_ node : Node?) -> Node?{
        var current = first;
        while (current != nil) {
            if (current?.next?.value == node?.value) { return current };
            current = current?.next;
        }
        return nil;
      }
    
     func getsize() -> Int {
        return size;
      }
    
    func toArray() -> [Int]{
        var array : Array<Int> = Array(repeating: 0, count: size)
        var current = first;
        var index = 0;
        while (current != nil) {
          array[index] = current?.value ?? 0;
          index += 1
          current = current?.next;
        }
        return array;
    }
    
    func reverse() {
         //   [10 ->20 -> 30 -> 40]
         //1.  p    c
         //2.  c.next = p
         //    20 -> 10
         //3.  n = c.next
         //               n
         //               p       c
        
        
        if (isEmpty()) {return};
        
        var previous = first;
        
       
        var current = first?.next;
    

         while (current != nil) {
             
             let next = current?.next;
             //30
             //40
             current?.next = previous;
             //10
             //20
             previous = current;
             //20
             //30
             current = next;
             //30
             //40
         }
        
        last = first;
        last?.next = nil;
        first = previous;
    }
    
    
    func getKthFromTheEnd(k : Int) -> Int{
        if (isEmpty()) {
        print("IllegalStateException")
        return -1
        }
        
        var a = first;
        var b = first;
        
        for _ in 0..<k-1 {
            b = b?.next;
            if (b == nil) {
                print("IllegalArgumentException")
                break
            }
         }
        while (b?.value != last?.value) {
               a = a?.next;
               b = b?.next;
        }
        return a?.value ?? -1
    }
    
   func printMiddle() {
        if (isEmpty()) {
            print("IllegalStateException")
            return
        }
      

        var a = first;
        var b = first;
         while (b?.value != last?.value && b?.next?.value != last?.value) {
          b = b?.next?.next;
          a = a?.next;
        }

     if (b?.value == last?.value) {
         print(a?.value ?? -1)
    } else {
        print("\(a?.value ?? -1) + ", " + \(a?.next?.value ?? -1)")
    }
 }
    
    func hasLoop() -> Bool {
        var slow = first;
        var fast = first;

        while (fast != nil && fast?.next != nil) {
          slow = slow?.next;
          fast = fast?.next?.next;

            if (slow?.value == fast?.value) {
                return true;
            }
            
        }

        return false;
      }
    
    
}

func createWithLoop() -> LinkedList {
   let list = LinkedList();
   list.addLast(10);
   list.addLast(20);
   list.addLast(30);

   // Get a reference to 30
    let node = list.last;

   list.addLast(40);
   list.addLast(50);

   // Create the loop
    list.last?.next = node;

   return list;
 }
func testLinkedList() {
    let item : LinkedList = LinkedList()
    item.addLast(1)
    item.addLast( 3)
    item.addLast( 5)
    item.addLast( 7)
    item.addLast(  9)
    item.addLast(  11)
    item.setNode(8, 2)
    print(item.printLL)
//    item.removeLast()
//    print(item.getKthFromTheEnd(k : 6))
//    print(item.toArray())
//    print(item.printMiddle())
//    print(item.toArray())
   // print( createWithLoop())
//    item.reverse()
//    print(item.toArray())
//    print(item.size)
}
