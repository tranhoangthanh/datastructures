//
//  Node.swift
//  DataStructures
//
//  Created by thanh on 24/06/2021.
//

import Foundation


class Node {
    var value : Int
    var next : Node?

    init(_ value : Int) {
        self.value = value
    }
  }
