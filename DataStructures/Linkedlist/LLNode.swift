//
//  LLNode.swift
//  DataStructures
//
//  Created by thanh on 27/06/2021.
//

import Foundation

class LLNode<T> {
    var value : T
    var next : LLNode?
    var previous : LLNode?
    
    init(value : T) {
        self.value = value
    }
}


