//
//  DLinkedList.swift
//  DataStructures
//
//  Created by thanh on 27/06/2021.
//

import Foundation

class DLinkedList<T> {
    typealias DNode = LLNode<T>
    
    var head : DNode?
    
    var first : DNode? {
        return head
    }
    
    var last : DNode? {
        guard var node = head else {
            return nil
        }
        while let next = node.next {
            node = next
        }
        return node
    }
    
    var count : Int {
        guard var node = head else {
            return 0
        }
        var count = 1
        while let next = node.next {
            node = next
            count += 1
        }
        return count
    }
    
    func append(_ value : T) {
        let newNode = DNode(value: value)
        if let lastNode = last {
            newNode.previous = lastNode
            lastNode.next = newNode
        } else {
            head = newNode
        }
    }
    
    func node(_ index : Int) -> DNode? {
        if index == 0 {
            return head
        } else {
            var node = head?.next
            for _ in 1..<index {
                node = node?.next
                if node == nil {break}
            }
            return node
        }
        
    }
    
    var print : String {
        var stringArray = "["
        guard var node = head else {
            return stringArray + "]"
        }
        
        while let next = node.next {
            stringArray += "\(node.value),"
            node = next
        }
        
        stringArray += "\(node.value)"
        
        return stringArray + "]"
        
    }
    
    func insertNode(_ value : T , _ index : Int) {
        let newNode = DNode(value: value)
        if index == 0 {
            newNode.next = head
            head?.previous = newNode
            head = newNode
        } else {
            let prev = self.node(index-1)
            let next = prev?.next
            newNode.previous = prev
            newNode.next = prev?.next
            
            prev?.next = newNode
            next?.previous = newNode
        }
    }
    
    
    func removeAt(node : DNode) -> T {
        let prev = node.previous
        let next = node.next
        if let prev = prev {
            prev.next = next
        } else {
            head = next
        }
        
        next?.previous = prev
        node.previous = nil
        node.next = nil
        return node.value
    }
    
    
}

func testDLinkedList() {
    let item : DLinkedList = DLinkedList<Int>()
    item.append(1)
    item.append( 3)
    item.append( 5)
    item.append( 7)
    item.append( 9)
    item.append( 11)
    item.insertNode(8, 2)
    print(item.print)
}
