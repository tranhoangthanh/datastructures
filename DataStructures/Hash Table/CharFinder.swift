//
//  CharFinder.swift
//  DataStructures
//
//  Created by thanh on 27/06/2021.
//

import Foundation


class CharFinder {
  
    func findFirstNonRepeatingChar(_ str : String) -> String {
        var map : [String : Int] = [:]
        let array = str.map( { String($0) })
        for ch in array  {
            let count = map.contains(where: { (key, value) -> Bool in
                key as String == ch
            }) ? map[ch]! : 0
            map.updateValue(count+1, forKey: ch)
        }
        for ch in array {
            if map[ch] == 1 {
                return ch
            }
        }
        return ""
    }
    
    func findFirstRepeatedChar(_ str : String) -> String {
        var set = Set<String>()
        let array = str.map( { String($0) })
        for ch in array  {
            if set.contains(where: { value in
                value == ch
            }) {
                return ch
            } else {
                set.update(with: ch)
            }
        }
        
        return ""
    }
    
    
    
    
    
}

func testFinder(){
    let finder = CharFinder()
    print(finder.findFirstNonRepeatingChar("a green apple"))
    print(finder.findFirstRepeatedChar("green apple"))
    
}
