//
//  Entry.swift
//  DataStructures
//
//  Created by thanh on 27/06/2021.
//

import Foundation

struct Entry {
    var key : Int
    var value : String
    
    init(key : Int , value : String) {
        self.key = key
        self.value = value
    }
}
