//
//  TwoStacks.swift
//  DataStructures
//
//  Created by TranHoangThanh on 6/23/21.
//

import Foundation

class TwoStacks {
    var top1 : Int = 0
    var top2 : Int = 0
    var items : Array<Int> = Array(repeating: 0, count: 0)
    
    init(capacity : Int) {
        if capacity < 0 {
            print("capacity must be 1 or greater.")
            return
        }
        items = Array(repeating: 0, count: capacity)
        top1 = -1;
        top2 = capacity
    }
    
    func push1(item : Int) {
        if (isFull1()) {
            print("IllegalStateException")
            return
        }
        top1 += 1
        items[top1] = item;
    }
    func setValueIndex(value : Int) {
        for i in 0...items.count-1 {
            if items[i] == value {
                items[i] = 0
            }
        }
    }
    func pop1() -> Int {
        if (isEmpty1()){
            print("IllegalStateException")
            return -1
        }
        let value = items[top1]
        self.setValueIndex(value: value)
        top1 -= 1
        return value
    }
    
    func push2(item : Int) {
        if (isFull2()) {
            print("IllegalStateException")
        }
        top2 -= 1
        items[top2] = item;
    }
    
    func pop2() -> Int {
        if (isEmpty2()) {
            print("IllegalStateException")
            return -1
        }
        let value = items[top2]
        self.setValueIndex(value: value)
        top2 += 1
        return value
    }
    
    func isEmpty1() -> Bool {
        return top1 == -1;
    }
    
    func isFull1() -> Bool {
        return top1 + 1 == top2;
    }
    
    func  isEmpty2() -> Bool {
        return top2 == items.count
    }
    
    func isFull2() -> Bool {
        return top2 - 1 == top1;
    }
}

func testTwoStacks() {
    let stack = TwoStacks(capacity: 8)
    stack.push1(item: 1)
    stack.push1(item: 2)
    stack.push2(item: 3)
    stack.push2(item: 4)

    print(stack.pop1())
    print(stack.items)
}
