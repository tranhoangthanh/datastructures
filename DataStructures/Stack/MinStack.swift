//
//  MinStack.swift
//  DataStructures
//
//  Created by TranHoangThanh on 6/23/21.
//

import Foundation

class MinStack {
    let stack = Stack(length: 5);
    let minStack =  Stack(length: 5);
    
    
    func push(item : Int) {
        print("stack.push",item)
        stack.push(item: item);
        if (minStack.isEmpty()) {
            print("minStack.isEmpty",item)
            minStack.push(item: item);
        } else if (item < minStack.peek()) {
            print("minStack.push",item)
            minStack.push(item: item);
        }
    }
    
    func pop() ->  Int {
        if (stack.isEmpty()) {
            print("IllegalStateException")
            return -1
        }
        let top = stack.pop();
        if (minStack.peek() == top) {
            print(minStack.pop())
        }
        return top;
    }
    
    func min() -> Int {
        return minStack.peek();
    }
}

func testMinStack(){
    let stack = MinStack()
    stack.push(item: 5)
    stack.push(item: 4)
    stack.push(item: 10)
    stack.push(item: 3)
    stack.push(item: 1)
    
    
    stack.min()
    stack.pop()
    // stack.pop()
    print(stack.min())
}
