//
//  Stack.swift
//  DataStructures
//
//  Created by TranHoangThanh on 6/23/21.
//

import Foundation

enum StackError: Error {
    case StackOverflowError
    case IllegalStateException
}

class Stack {
    var items : Array<Int> 
    var count = 0
    
    init(length : Int) {
        items = Array(repeating: 0, count: length)
    }
    
    func push(item : Int)   {
        if count == items.count {
            print("StackOverflowError")
            return
        }
        items[count] = item
        count += 1
    }
    func setValueIndex(value : Int) {
        for i in 0...items.count-1 {
            if items[i] == value {
                items[i] = 0
            }
        }
    }
    func pop() -> Int {
        if count == 0 {
            print("IllegalStateException")
            return -1
        }
        count -= 1
       // print("count",count)
        let value =  items[count]
        //setValueIndex(value: value)
        return value
    }
    
    func peek() -> Int {
        if count == 0 {
            print("IllegalStateException")
            return -1
        }
      //  print("count-1",count-1)
        let value =  items[count-1]
        return value
    }
    
    func isEmpty() -> Bool {
        return count == 0
    }
}

func testStack() {
    let stack = Stack(length: 5)
         stack.push(item: 1)
         stack.push(item: 2)
         stack.push(item: 3)
        //print(try stack.peek())
       // print(try stack.pop())
       // try stack.push(item: 4)
    
    print(stack.items)
}
